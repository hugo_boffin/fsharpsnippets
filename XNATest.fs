﻿module XNATest

open System
open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Content
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework.Input

type Spiel () as this =
    inherit Game ()
    let graphics = new GraphicsDeviceManager (this)
    do
        Console.WriteLine "Neues Spiel"
        graphics.PreferredBackBufferWidth <- 1024
        graphics.PreferredBackBufferHeight <- 600
        graphics.GraphicsProfile <- GraphicsProfile.HiDef
        graphics.SynchronizeWithVerticalRetrace <- false
        this.IsMouseVisible <- true
        this.IsFixedTimeStep <- false

    override Game.Initialize () =
        this.Window.Title <- "EE64 - Powered by Cimoyok.nextgen"
        this.Window.AllowUserResizing <- true
        this.Window.ClientSizeChanged.AddHandler( fun sender args -> (this.Window_ClientSizeChanged sender args) )
        this.Content.RootDirectory <- "Content"
        base.Initialize ()
        
    override Game.LoadContent () =
        base.LoadContent ()

    override Game.UnloadContent () =
        base.UnloadContent ()
        
    override Game.Update gameTime =
        base.Update gameTime

    override Game.Draw gameTime =
        this.GraphicsDevice.Clear Color.CornflowerBlue
        base.Draw gameTime
        
    member this.Window_ClientSizeChanged sender args =
        Console.WriteLine "Size Changed"

let spiel = new Spiel ()
try spiel.Run ()
finally spiel.Dispose ()
