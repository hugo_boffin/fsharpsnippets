﻿module Simple

open System
open System.Windows.Forms

let duration f = 
    let startTime = DateTime.Now;
    let returnValue = f()
    let endTime = DateTime.Now;
    printfn "Duration (ms): %f" (endTime - startTime).TotalMilliseconds
    returnValue

let rec slowMultiply a b =
    if b > 1 then
        let intermediate = slowMultiply a (b - 1) (* recursion *)
        let result = a + intermediate (* <-- additional operations *)
        result
    else a

let slowMultiply_tail a b =
    let rec loop acc counter =
        if counter > 1 then
            loop (acc + a) (counter - 1) (* tail recursive *)
        else
            acc
    loop a b
    
//printfn "slowMultiply: %i" (duration ( fun () -> slowMultiply 50000 5000 ))
//printfn "slowMultiply_tail: %i" (duration ( fun () -> slowMultiply_tail 50000 50000 ))

let rec fib = function
    | n when n=0I -> 0I
    | n when n=1I -> 1I
    | n -> fib(n - 1I) + fib(n - 2I)

let fib_tail n =
    let rec loop acc1 acc2 n =
        match n with
        | n when n=0I -> acc1
        | n -> loop acc2 (acc1 + acc2) (n - 1I)
    loop 0I 1I n

//Console.WriteLine (fib 100I)
Console.WriteLine (fib_tail 0I)
Console.WriteLine (fib_tail 1I)
Console.WriteLine (fib_tail 2I)
Console.WriteLine (fib_tail 100I)

let x = 5
let y = 10
let z = x + y
 
//printfn "x: %i" x
//printfn "y: %i" y
//printfn "z: %i" z

Console.ReadKey true |> ignore

(*
(* Create a window and set a few properties *)
let form = new Form(Visible=true, TopMost=true, Text="EE64 - Powered by Cimoyok.nextgen")
 
(* Create a label to show some text in the form *)
let label =
    let temp = new Label()
    let x = 3 + (4 * 5)
    (* Set the value of the Text*)
    temp.Text <- sprintf "x = %d" x
    (* Remember to return a value! *)
    temp

form.Controls.Add(label)

[<STAThread>]
Application.Run(form)
*)